from main import app
import pytest
from flask import Flask, redirect,render_template, request, jsonify,url_for, session,escape
from project import config
from project.userInfo import user
import requests
import json

app.secret_key = "molon labe for testing"

@pytest.mark.parametrize('url', [
	('/'),
	('/marketSummaries'),
	('/specificMarketSummary'),
	('/login'),
	('/sign_up')
	])
def test_app_urls(url):
	tester = app.test_client()
	response = tester.get(url) 
	assert response.status_code == 200


def test_app_urls_302():
	tester = app.test_client()
	response = tester.get('/logout') 
	assert response.status_code == 302

def test_home_with_session():
	tester = app.test_client()
	with tester.session_transaction() as session:
		session['email'] = 'chris.kardambikis@gmail.com'
	url = '/'
	response = tester.get(url)
	assert response.status_code == 200

# testing specific market summaries link
def test_specificMarketSummary_mock_args():
	tester = app.test_client()
	with tester.session_transaction() as session:
		session['email'] = 'chris.kardambikis@gmail.com'
	url = '/specificMarketSummary'
	response = tester.get(url,query_string={'market': 'btc-ltc'}) 
	assert response.status_code == 200

# testing all market summaries link
def test_marketSummaries():
	tester = app.test_client()
	with tester.session_transaction() as session:
		session['email'] = 'chris.kardambikis@gmail.com'
	url = '/marketSummaries'
	response = tester.get(url) 
	assert response.status_code == 200

# testing login
def test_login():
	tester = app.test_client()
	url = '/login'
	response = tester.post(url, data={'email': 'chris.kardambikis@gmail.com', 'password':'1234567'})
	assert response.status_code == 302

def test_login_wrong_pass():
	tester = app.test_client()
	url = '/login'
	response = tester.post(url, data={'email': 'chris.kardambikis@gmail.com', 'password':'123456'})
	assert response.status_code == 200

def test_login_non_exist():
	tester = app.test_client()
	url = '/login'
	response = tester.post(url, data={'email': 'chris.kardambikis@gmail', 'password':'123456'})
	assert response.status_code == 200

# Testing sign_up
def test_sign_up_exist():
	tester = app.test_client()
	url = '/sign_up'
	response = tester.post(url, data={'email': 'chris.kardambikis@gmail.gr','first_name':'Chris','password1':'1234567','password2':'1234567'})
	assert response.status_code == 200

def test_sign_up_non_exist():
	tester = app.test_client()
	url = '/sign_up'
	response = tester.post(url, data={'email': 'chris.kardambikis@gmail.co.com.gr','first_name':'Chris','password1':'1234567','password2':'1234567'})
	assert response.status_code == 302

def test_sign_up_small_email():
	tester = app.test_client()
	url = '/sign_up'
	response = tester.post(url, data={'email': 'chr','first_name':'Chris','password1':'1234567','password2':'1234567'})
	assert response.status_code == 200

def test_sign_up_pass1_not_pass2():
	tester = app.test_client()
	url = '/sign_up'
	response = tester.post(url, data={'email': 'chris.kardambikis@gmail','first_name':'Chris','password1':'123456','password2':'1234567'})
	assert response.status_code == 200

def test_sign_up_small_pass():
	tester = app.test_client()
	url = '/sign_up'
	response = tester.post(url, data={'email': 'chris.kardambikis@gmail','first_name':'Chris','password1':'123','password2':'123'})
	assert response.status_code == 200

# testing logout
def test_logout():
	tester = app.test_client()
	url = '/logout'
	response = tester.get(url)
	assert response.status_code == 302


