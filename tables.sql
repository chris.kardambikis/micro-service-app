DROP TABLE IF EXISTS `MarketSummary`;
CREATE TABLE `MarketSummary` (
  `MarketName` varchar(500) NOT NULL,
  `High` float(30),
  `Low` float(30) ,
  `Volume` float(30) ,
  `Last` float(30) ,
  `BaseVolume` float(30),
  `TimeStamp` varchar(500),
  `Bid` float(30),
  `Ask` float(30),
  `OpenBuyOrders` int,
  `OpenSellOrders` int,
  `PrevDay` float(30),
  `Created` varchar(500),
  PRIMARY KEY (`MarketName`)
);

DROP TABLE IF EXISTS `MarketUsers`;
CREATE TABLE `MarketUsers` (
  `Email` varchar(500) NOT NULL,
  `First_Name` varchar(500) NOT NULL,
  `Pasword` varchar(500) NOT NULL,
  PRIMARY KEY (`Email`)
);