from project import app
from project.userInfo import user
from project.database import database
from flask import Flask, Response,redirect,render_template, request, jsonify,url_for, session,escape
import requests
import json

app.secret_key = "molon labe"

# login function
@app.route('/login', methods=['GET', 'POST'])
def login():
	args={"id": "login", "title": "Micro Service App"}
	if_exists = True
	usr_values=[]

	if request.method == 'POST':
		email = request.form.get('email')
		password = request.form.get('password')
		usr = app.config['User']
		usr.email = email
		usr.password=password
		if_exists,usr_values = usr.check_if_exist(usr.email)

		if if_exists:
			if usr_values["password"] == usr.password:
				# save the users first name
				usr.first_name = usr_values["first_name"]
				info = usr.first_name
				# session
				session["email"] = usr.email
				
				return redirect(url_for('home'))				
			else:
				args["error_message"] = 'Incorrect password, try again.'
				return render_template("login.html",args=args)
		else:
			args["error_message"] = 'Email does not exist.'
			return render_template("login.html",args=args)
	return render_template("login.html",args=args)

#  logout function
@app.route('/logout')
def logout():
	args={"id": "logout", "title": "Micro Service App"}
	session.pop('email', None)
	return redirect(url_for('home'))
	# return render_template("login.html",args=args)

# sign-up function
@app.route('/sign_up', methods=['GET', 'POST'])
def sign_up():
	args={"id": "sign_up", "title": "Micro Service App"}
	args["is_authenticated"] = False
	if request.method == 'POST':
		email = request.form.get('email')
		first_name = request.form.get('first_name')
		password1 = request.form.get('password1')
		password2 = request.form.get('password2')
		success =""
		args["error_message"]=""
		if_exists = True
		usr_values=[]
		usr = app.config['User']

		if_exists,usr_values = usr.check_if_exist(email)

		if if_exists:
			args["error_message"] = 'Email already exists.'
			return render_template("sign_up.html",args=args)
		elif len(email) < 4:
			args["error_message"] = 'Email must be greater than 3 characters.'
			return render_template("sign_up.html",args=args)
		elif password1 != password2:
			args["error_message"] = 'Password must be the same.'
			return render_template("sign_up.html",args=args)
		elif len(password1) < 7:
			args["error_message"] = 'Password must be at least 7 characters.'
			return render_template("sign_up.html",args=args)
		else:
			usr.email = email
			usr.first_name = first_name
			usr.password = password1
			success = usr.save_to_db()
			# session
			session["email"] = usr.email

			return redirect(url_for('home'))

	return render_template("sign_up.html",args=args)

def get_session_user():
	usr = app.config['User']
	if 'email' not in session:
		return None
	email = session['email']
	user = usr.get_user_by_email(email)
	return user

@app.route("/")
def home():
	usr_session = app.config['User']
	user = get_session_user()
	if user:
		usr_session.email = user["email"]
		usr_session.first_name = user["first_name"]
		args={"id": "msa", "title": "Micro Service App", "first_name": usr_session.first_name}
	else:
		args={"id": "msa", "title": "Micro Service App"}
		
	return render_template('msa.html', args=args)


@app.route("/marketSummaries")
def all_market_summaries():
	db = app.config['MarketSummaries']
	ac = app.config['ApiCalls']

	# check if session
	usr_session = app.config['User']
	user = get_session_user()
	if user:
		usr_session.email = user["email"]
		usr_session.first_name = user["first_name"]
		args={ "id": "marketSummaries", "title": "All Market Summaries", "first_name": usr_session.first_name}

		args["headers"] = ["Market Name","High","Low","Volume","Last","Base Volume", "Time Stamp","Bid", " Ask", "Open Buy Orders","Open Sell Orders", " Prev. Day", "Created"]	
		args["marketSummary"] = ""
		args['success_insert'] =""
		args['success'] = True

		# first api request to retrieve all market summaries
		json_data,args['success'] = ac.all_market_summaries_api()

		# If successful, response 200   
		if args['success']:
			# insert to database the data
			rowcount = db.insert_to_database(json_data["result"])
			if rowcount > 0:
				args['success_insert'] = "This was a success!!"
			else:
				args['success_insert'] = "This was NOT a success!!"
			# retrieve from database the data
			args["marketSummary"] = db.get_marketSummaries_from_database()
		else:
			# if not successful save the error code
			args['success_insert'] = json_data

		return render_template('marketSummaries.html', args=args)
	else:
		args={"id": "msa", "title": "Micro Service App"}
		return render_template('msa.html', args=args)


@app.route("/specificMarketSummary")
def specific_market_summary():
	# the API authentication without the param.
	db = app.config['MarketSummaries']
	ac = app.config['ApiCalls']

	# check if session
	usr_session = app.config['User']
	user = get_session_user()
	if user:
		usr_session.email = user["email"]
		usr_session.first_name = user["first_name"]
	
		args={ "id": "specificMarketSummary", "title": "Specific Market Summary", "first_name": usr_session.first_name}
		args["headers"] = ["Market Name","High","Low","Volume","Last","Base Volume", "Time Stamp","Bid", " Ask", "Open Buy Orders","Open Sell Orders", " Prev. Day", "Created"]	
		args["market"] = ""
		args["marketSummary"]=""
		args["message"]=""
		args['success_insert'] =""
		args['success'] = True

		# get the param.
		if "market" in request.args:
			args["market"] = request.args.get("market")

			# second api request to retrieve the specific market summary
			json_data,message,args['success']= ac.market_summary_api(args["market"])
			
			# check if we got a success response
			if args["success"]:
				args["marketSummary"] = db.getValuesFromResults(json_data["result"])	
			else:
				if message:
					args["message"] = json_data["message"]
				else:
					# if not successful save the error code
					args['success_insert'] = json_data


		return render_template('specificMarketSummary.html', args=args)
	else:
		args={"id": "msa", "title": "Micro Service App"}
		return render_template('msa.html', args=args)
