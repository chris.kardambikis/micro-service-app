import requests
import json
import mysql.connector

class ApiCalls:
	def __init__(self):
		self.url_req=""

	def all_market_summaries_api(self):

		try:
			response = requests.get("https://api.bittrex.com/api/v1.1/public/getmarketsummaries")
		except KeyError:
			print("There has been a problem connecting to server")

		if response.status_code == 200:
			return json.loads(response.text), True
		else:
			return response.status_code, False

	def market_summary_api(self,market):

		str_request ="https://api.bittrex.com/api/v1.1/public/getmarketsummary?market="

		try:
			# add the param to the authentication url
			str_request += market
			response = requests.get(str_request)
		except KeyError:
			print("There has been a problem connecting to server")

		if response.status_code == 200:
			text = json.loads(response.text)
			return text,text["message"],text["success"]
		else:
			return response.status_code,'',False




class MarketSummaries:
	def __init__(self):
		self.MarketName = []
		self.High = []
		self.Low = []
		self.Volume = []
		self.Last = []
		self.BaseVolume = []
		self.TimeStamp = []
		self.Bid = []
		self.Ask = []
		self.OpenBuyOrders = []
		self.OpenSellOrders = []
		self.PrevDay = []
		self.Created = []


	def insert_to_database (self, json_data_results):
		# connect to the database
		mydb = mysql.connector.connect( 
			host="localhost",
			user="root", 
			password="12345678", 
			database="MarketSummaries"
			)
		mycursor = mydb.cursor()
		# delete any previous (old) data from table
		sql = "DELETE FROM MarketSummary"
		mycursor.execute(sql)
		mydb.commit()

		# build the sql query
		sql = "INSERT INTO MarketSummary(MarketName, High,Low,Volume,Last,BaseVolume,TimeStamp,Bid,Ask,OpenBuyOrders,OpenSellOrders,PrevDay,Created) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

		# get all values from the json dic.
		val=[]
		for row in json_data_results:
			tmp_val=[]
			for i in row:
				tmp_val.append(row[i])
			val.append(tmp_val)

		# prepare and commit the query for mysql database
		mycursor.executemany(sql, val)
		mydb.commit()
		# print if success
		return mycursor.rowcount

	def get_marketSummaries_from_database(self):
		
		# connect to the database
		mydb = mysql.connector.connect( 
			host="localhost",
			user="root", 
			password="12345678", 
			database="MarketSummaries"
			)

		mycursor = mydb.cursor()
		mycursor.execute("SELECT * FROM MarketSummary")
		myresult = mycursor.fetchall()

		return myresult

	def getValuesFromResults(self,json_data_results):
		# get all values from the json dic.
		val=[]
		for row in json_data_results:
			tmp_val=[]
			for i in row:
				tmp_val.append(row[i])
			val.append(tmp_val)
		return val






