from project import app
from project.database import database
from project.userInfo import user
# import sys
# import os

db = database.MarketSummaries()
app.config['MarketSummaries'] = db
ac = database.ApiCalls()
app.config['ApiCalls'] = ac
usr = user.User()
app.config['User'] = usr

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=9292)
