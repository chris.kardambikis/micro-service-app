# A micro service app to fetch crypto currency market updates


INTRODUCTION
------------

I was asked to build a micro service app to fetch crypto currency market updates from " https://api.bittrex.com ".

Functional Tasks:

*	API implementation and param validation using standard libraries.

*	One API to return all market summary, backend can refer to the below with authentication.

	- " https://api.bittrex.com/api/v1.1/public/getmarketsummaries "

*	One API which accepts market as query param and depends on the below backend API with authentication. 

	- " https://api.bittrex.com/api/v1.1/public/getmarketsummary?market=btc-ltc "

*	Quality unit tests using pytest

	- Coverage should be above 80%

*	Code should be committed to Git and shared for us to be able to monitor commit messages and evolution of code from scratch.

	- Helps us evaluate development approach from you.
  
Non Functional Tasks:

*	Protect the APIs developed using standard authentications and authorisation techniques. 

*	Professional API documentation. (ex: swagger) 

*	Linting and quality control integrated in the  in the project folder.

*	Good project structuring

*	Secured way of storing auth credentials

*	No Hardcoding 

*	Efficient code (less loops, conditional statements)

*	Read.md with project documentation

Bonus tasks:

*	Docker build files

*	Back end Health check and version info APIs.

*   Store user responses to a mongo DB collection for audit purposes.


REQUIREMENTS
------------

Need to install:

* Python (latest verison)
* pip 
* flask -> $ pip install Flask
* pytest -> $ pip install pytest
* coverage -> $ pip install coverage
* Mysql

CONFIGURATION
-------------

* You will need to configure the database in MySQL.

    - You will need to create a new database called "MarketSummaries".

    - You will need set the database username: "root" and password: "12345678".

    - Run the "tables.sql" file (You will find it in the micro_service_app folder) to create two tables (MarketSummary, MarketUsers).
        
    - As a result, you should have one database (MarketSummaries) with 2 tables (MarketSummary, MarketUsers).


HOW TO USE
-------------

* Running the application.

The code to setup and run the application is in the src/main.py file.

In a terminal window go to the directory into which you just cloned the code. You should see two directories: src and components. Add the src directory to the PYTHONPATH environment variable so that Python can find the application code. How you do this will be different on different platforms but on Linux machines do:
 		
	- " export PYTHONPATH=src "

You'll need to do this in each new terminal window that you open.

This application uses Python 3, so make sure that you don't accidentally run it with Python 2. On some machines with both Python 2 and 3 installed you will need to explicitly specify python3 when running Python.

Then run the application with:

	- " python3 src/main.py "

This starts a Web server listening on port 9292 so point your Web browser to " http://localhost:9292 " to use the application.

* Once up and running, Go to your browser and paste: " http://localhost:9292 "
  
* You should be able now to see the Home Page.
  
* From the Home Page you can login or sign up (if first time).
  
* Once signed up or login you will have access to the two services:

	- Get All Market Summaries

	- Get specific Market Summaries

* Get All Market Summaries

Returns all market summary from " https://api.bittrex.com ". 
  
* Get specific Market Summaries

Returns all market summary from " https://api.bittrex.com " for a specific market value (ex. " btc-ltc").
  
* You can always navigate to the Home page or Logout.
    

TROUBLESHOOTING
---------------

* If " http://localhost:9292 " does not load, check the following:

    - Make sure your MySQL is up and running with the correct Tables.

    - Make sure your MySQL username:root and password: 12345678.

    - Reload project (close terminal and make sure you are in the correct folder when you export PYTHONPATH=src) 

FAQ
---

Q: Do I need to populate the tables before running the app?

A: No, the tables do not need to be populated. The data will come from the app.

Q: Is there a way to check my password?

A: No there is not. Yet, maybe in the future..


MAINTAINERS
-----------

Current maintainers:
* Christos Kardambikis - email: christos.kardambikis@gmail.com